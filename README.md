0. Installer VirtualBox 

( Pour les étapes -0/1/2 et 3- voir l'exemple sur la vidéo ci-dessous):

    => https://youtu.be/Evf93C-AtHI?si=ZLaMhye0RHLRfXob

1. Telecharger HomeServer VM forma .vbox:

    => j'ajoute le lien après ;)
    
2. Changer la configuration réseau sur VirtualBox en  (Accès par Pont).
3. Ouvrir la VM.
4. VM contienne: 

    | Application / Outil  | Son URL sur ta machine | Login/Pasword |
    |--|--|--|
    | Portainer | https://homeserver:9443/ | (admin) / (*****) |
    | OpenmediaVault (OMV) | http://homeserver/ | (admin) / (openmediavault) |
    | NextCloud (Ton Drive) :) | https://homeserver:9088/ | (admin) / (*****) |

5. (Facultatif) Connexion à Internet:
    - Dynamic DNS avec DuckDNS
    
    OR

    - IoT Connexion avec : remote.it

